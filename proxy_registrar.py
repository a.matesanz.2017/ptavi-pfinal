# !/usr/bin/python3

import hashlib
import json
import random
import socket
import socketserver
import sys
import time

from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class ProxyRegistrarHandler(ContentHandler):
    """
    Clase para manejar los elementos de los ficheros xml
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """

        # los elementos siguen la sintaxis descrita en el DTD
        self.server_list = ['name', 'ip', 'puerto']
        self.database_list = ['path', 'passwdpath']
        self.log_list = ['path']

        self.base_pr_dictionary = {'server': self.server_list,
                                   'database': self.database_list,
                                   'log': self.log_list
                                   }

        self.pr_dictionary = {}

    def startElement(self, name, attrs):
        """
        Introducimos los elementos del fichero xml en el diccionario
        """

        pr_dictionary = {}
        if name in self.base_pr_dictionary:
            for attrib in self.base_pr_dictionary[name]:
                n_a = name
                n_a += '_'
                n_a += attrib
                self.pr_dictionary[n_a] = attrs.get(attrib, '')

    def get_tags(self):
        return self.pr_dictionary


class SIPRegisterHandler(socketserver.DatagramRequestHandler):

    register_dictionary = {}
    passwords_dictionary = {}
    nonce_dictionary = {}

    def json2password(self):

        """
        Rellenamos el diccionario con el contenido del json
        Abrimos un fichero para leer ('r' de Read)
        Da error si no existe el archivo, por eso hay que poner la excepcion
        """
        try:

            with open(DATABASE_PASSWDPATH, 'r') as filee:
                loaded_file = json.load(filee)
                self.passwords_dictionary = loaded_file
        except FileNotFoundError:
            pass

    def register2json(self):
        """
        Escribimos en el fichero el contenido del diccionario
        Abrimos un fichero para escribir ('w' de Write)
        Si el archivo no existe, lo crea
        """
        with open(DATABASE_PATH, 'w') as filee:
            json.dump(self.register_dictionary, filee, indent=4)

    def send_to_client(self, ip, port, line):

        # lo escribimos en algo
        byted_line = bytes(line, 'utf-8')
        self.wfile.write(byted_line)

        # lo escribimos en el fichero de log
        corrected_line = line.replace('\r\n', ' ')
        line_to_write = 'Sent to '
        line_to_write += ip
        line_to_write += ':'
        line_to_write += str(port)
        line_to_write += ': '
        line_to_write += corrected_line
        write_log(line_to_write, LOG_PATH)

        # lo mostramos por pantalla
        tuplee = '(IP address: '
        tuplee += ip
        tuplee += ', Port number: '
        tuplee += str(port)
        tuplee += ')'
        stripped_line = line.lstrip()
        line_to_show = '   Sent to UA '
        line_to_show += tuplee
        line_to_show += ':'
        print('\r\n\r\n' + line_to_show)
        print('\r\n' + stripped_line)

    def send_to_server(self, ip, port, line):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sockett:
            sockett.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sockett.connect((ip, port))
            splitted_line = line.split('\r\n\r\n')
            via_line = splitted_line[0]
            via_line += '\r\n'
            via_line += 'Via: SIP/2.0/UDP '
            via_line += SERV_IP
            via_line += ':'
            via_line += str(SERVER_PUERTO)
            via_line += '\r\n\r\n'
            via_line += splitted_line[1]

            byted_via_line = bytes(via_line, 'utf-8')
            sockett.send(byted_via_line)

            # lo escribimos en el fichero de log
            corrected_via_line = via_line.replace('\r\n', ' ')
            line_to_write = 'Sent to '
            line_to_write += ip
            line_to_write += ':'
            line_to_write += str(port)
            line_to_write += ': '
            line_to_write += corrected_via_line
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            tuplee = '(IP address: '
            tuplee += ip
            tuplee += ', Port number: '
            tuplee += str(port)
            tuplee += ')'
            stripped_via_line = via_line.lstrip()
            line_to_show = '   Sent to UA '
            line_to_show += tuplee
            line_to_show += ':'
            print('\r\n\r\n' + line_to_show)
            print('\r\n' + stripped_via_line)

            try:
                recv_data = sockett.recv(1024)
                decoded_data = recv_data.decode('utf-8')

                tuplee = '(IP address: '
                tuplee += ip
                tuplee += ', Port number: '
                tuplee += str(port)
                tuplee += ')'
                stripped_data = decoded_data.lstrip()
                line_to_show = '   Received from UA '
                line_to_show += tuplee
                line_to_show += ':'
                print('\r\n\r\n' + line_to_show)
                print('\r\n' + stripped_data)

            except ConnectionRefusedError:

                # lo escribimos en el fichero de log
                line_to_write = 'Error: No server listening at '
                line_to_write += ip
                line_to_write += ' port '
                line_to_write += str(port)
                write_log(line_to_write, LOG_PATH)

            splitted_aux = decoded_data.split()
            line_to_send = ''
            try:
                if splitted_aux[7] == '200':
                    splitted_data = decoded_data.split('\r\n\r\n')
                    line_to_send = splitted_data[0]
                    line_to_send += '\r\n\r\n'
                    line_to_send += splitted_data[1]
                    line_to_send += '\r\n\r\n'
                    line_to_send += splitted_data[2]
                    line_to_send += '\r\n'
                    line_to_send += 'Via: SIP/2.0/UDP '
                    line_to_send += SERV_IP
                    line_to_send += ':'
                    line_to_send += str(SERVER_PUERTO)
                    line_to_send += '\r\n\r\n'
                    line_to_send += splitted_data[3]
            except IndexError:
                line_to_send = decoded_data

            if line_to_send != '':

                # lo escribimos en el fichero de log
                replaced_line = line_to_send.replace('\r\n', ' ')
                line_to_write = 'Received from '
                line_to_write += ip
                line_to_write += ':'
                line_to_write += str(port)
                line_to_write += ': '
                line_to_write += replaced_line
                write_log(line_to_write, LOG_PATH)

                """
                Lo enviamos al cliente
                """
                self.send_to_client(ip, port, line_to_send)


    def handle(self):

        # con la tupla client_address obtenemos la IP y el puerto del cliente
        ip = self.client_address[0]
        CLIENT_IP_ADDRESS = str(ip)
        port = self.client_address[1]
        CLIENT_PORT_NUMBER = str(port)

        self.json2password()

        # bucle para escribir en el fichero de log
        # creamos e inicializamos la variable donde vamos a ir decodificando
        decoded_line = ''
        for line in self.rfile:

            # lo escribimos en el fichero de log
            decoded_line += line.decode('utf-8')
            linea_buena = decoded_line.replace('\r\n', ' ')
            line_to_write = 'Received from '
            line_to_write += CLIENT_IP_ADDRESS
            line_to_write += ':'
            line_to_write += CLIENT_PORT_NUMBER
            line_to_write += ': '
            line_to_write += linea_buena
            write_log(line_to_write, LOG_PATH)

            splitted_line = decoded_line.split()

        CONFIGG = splitted_line[0]

        if CONFIGG != 'REGISTER' and CONFIGG != 'INVITE' and CONFIGG != 'ACK' and CONFIGG != 'BYE':

            # lo escribimos en algo
            line = 'SIP/2.0 405 Method Not Allowed\r\n\r\n'
            byted_line = bytes(line, 'utf-8')
            self.wfile.write(byted_line)

            # lo escribimos en el fichero de log
            line_to_write = 'Error: SIP/2.0 405 Method Not Allowed'
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            line_to_show = '\r\n\r\nError: SIP/2.0 405 Method Not Allowed\r\n'
            print(line_to_show)

        elif CONFIGG == 'BYE' or CONFIGG == 'ACK':

            # lo escribimos en el fichero de log
            replaced_line = decoded_line.replace('\r\n', ' ')
            line_to_write = 'Received from '
            line_to_write += CLIENT_IP_ADDRESS
            line_to_write += ':'
            line_to_write += CLIENT_PORT_NUMBER
            line_to_write += ': '
            line_to_write += replaced_line
            write_log(line_to_write, LOG_PATH)

            # obtenemos la ip y el puerto del servidor
            aux = splitted_line[1].split(':')
            ua_server = aux[1]


            if ua_server in self.register_dictionary.keys():

                """
                Lo enviamos al servidor
                """
                destination_server_ip = self.register_dictionary[ua_server]['ip']
                destination_server_port = int(self.register_dictionary[ua_server]['puerto'])
                self.send_to_server(destination_server_ip, destination_server_port, decoded_line)

            else:

                # lo escribimos en algo
                line = 'SIP/2.0 404 User Not Found\r\n\r\n'
                byted_line = bytes(line, 'utf-8')
                self.wfile.write(byted_line)

                # lo escribimos en el fichero de log
                line_to_send = 'SIP/2.0 404 User Not Found\r\n\r\n'
                log_send = line_to_send.replace('\r\n', ' ')
                line_to_write = 'Error: '
                line_to_write += log_send
                write_log(line_to_write, LOG_PATH)

        elif CONFIGG == 'INVITE':

            # lo escribimos en el fichero de log
            replaced_line = decoded_line.replace('\r\n', ' ')
            line_to_write = 'Received from '
            line_to_write += CLIENT_IP_ADDRESS
            line_to_write += ':'
            line_to_write += CLIENT_PORT_NUMBER
            line_to_write += ': '
            line_to_write += replaced_line
            write_log(line_to_write, LOG_PATH)

            # obtenemos el nombre de usuario que manda el invite
            aux2 = splitted_line[6].split('=')
            user_name = aux2[1]

            # comprobamos si el usuario que manda el invite esta registrado
            if user_name in self.register_dictionary.keys():

                # obtenemos la ip y el puerto del servidor
                aux = splitted_line[1].split(':')
                ua_server = aux[1]


                # comprobamos si a quien va dirigido esta registrado
                if ua_server in self.register_dictionary.keys():

                    """
                    Lo enviamos al servidor
                    """
                    destination_server_ip = self.register_dictionary[ua_server]['ip']
                    destination_server_port = int(self.register_dictionary[ua_server]['puerto'])
                    self.send_to_server(destination_server_ip, destination_server_port, decoded_line)

                else:

                    # lo escribimos en algo
                    line = 'SIP/2.0 404 User Not Found\r\n\r\n'
                    byted_line = bytes(line, 'utf-8')
                    self.wfile.write(byted_line)

                    # lo escribimos en el fichero de log
                    line_to_send = 'SIP/2.0 404 User Not Found\r\n\r\n'
                    log_send = line_to_send.replace('\r\n', ' ')
                    line_to_write = 'Error: '
                    line_to_write += log_send
                    write_log(line_to_write, LOG_PATH)

            else:

                # lo escribimos en algo
                line = 'SIP/2.0 404 User Not Found'
                line += '\r\n\r\n'
                byted_line = bytes(line, 'utf-8')
                self.wfile.write(byted_line)

                # lo escribimos en el fichero de log
                line_to_send = 'SIP/2.0 404 User Not Found\r\n\r\n'
                log_send = line_to_send.replace('\r\n', ' ')
                line_to_write = 'Error: '
                line_to_write += log_send
                write_log(line_to_write, LOG_PATH)

        elif CONFIGG == 'REGISTER':

            if len(splitted_line) == 5 or len(splitted_line) == 8:

                # hora a la que va a expirar la conexion
                # es el tiempo de conexion mas la hora actual
                current_time = time.time()
                expiration_time = int(splitted_line[4]) + current_time

                # usuario y puerto
                aux = splitted_line[1].split(':')
                user_name = aux[1]
                port = aux[2]

                # lo escribimos en el fichero de log
                replaced_line = decoded_line.replace('\r\n', ' ')
                line_to_write = 'Received from '
                line_to_write += CLIENT_IP_ADDRESS
                line_to_write += ':'
                line_to_write += CLIENT_PORT_NUMBER
                line_to_write += ': '
                line_to_write += replaced_line
                write_log(line_to_write, LOG_PATH)

            # Es el primer intento de register si tiene longitud 5
            if len(splitted_line) == 5:

                # comprobamos si el usuario esta autorizado
                if user_name in self.passwords_dictionary.keys():

                    # comprobamos si el usuario esta registrado
                    if user_name in self.register_dictionary.keys():

                        # si se quiere registrar, el tiempo de expiracion sera diferente a 0
                        if splitted_line[4] != '0':
                            line_to_send = 'SIP/2.0 200 OK\r\n\r\n'
                            self.register_dictionary[user_name] = {'ip': CLIENT_IP_ADDRESS,
                                                   'expires': expiration_time,
                                                   'puerto': port,
                                                   'registro': current_time}


                            """
                            Lo enviamos al cliente
                            """
                            self.send_to_client(CLIENT_IP_ADDRESS, CLIENT_PORT_NUMBER, line_to_send)

                        else:
                            del self.register_dictionary[user_name]
                            line_to_send = 'SIP/2.0 200 OK\r\n\r\n'

                            """
                            Lo enviamos al cliente
                            """
                            self.send_to_client(CLIENT_IP_ADDRESS, CLIENT_PORT_NUMBER, line_to_send)

                    else:
                        self.nonce_dictionary[user_name] = str(random.randint(0, 100000000))
                        line_to_send = 'SIP/2.0 401 Unauthorized'
                        line_to_send += '\r\n'
                        line_to_send += 'WWW Authenticate: Digest nonce="'
                        line_to_send += self.nonce_dictionary[user_name]
                        line_to_send += '"\r\n\r\n'

                        """
                        Lo enviamos al cliente
                        """
                        self.send_to_client(CLIENT_IP_ADDRESS, CLIENT_PORT_NUMBER, line_to_send)

                else:
    
                    # lo escribimos en algo
                    line = 'SIP/2.0 404 User Not Found\r\n\r\n'
                    byted_line = bytes(line, 'utf-8')
                    self.wfile.write(byted_line)
    
                    # lo escribimos en el fichero de log
                    line_to_send = 'SIP/2.0 404 User Not Found\r\n\r\n'
                    line_to_send += ''
                    log_send = line_to_send.replace('\r\n', ' ')
                    line_to_write = 'Error: '
                    line_to_write += log_send
                    write_log(line_to_write, LOG_PATH)

            elif len(splitted_line) == 8:
    
                passw = self.passwords_dictionary[user_name]['passwd']

                # hallamos el digest_nonce
                hl_md5 = hashlib.md5()
                passwdd = bytes(passw, 'utf-8')
                hl_md5.update(passwdd)
                noncee = bytes(self.nonce_dictionary[user_name], 'utf-8')
                hl_md5.update(noncee)
                hl_nonce = hl_md5.hexdigest()

                # obtenemos el nonce para compararlos
                aux = splitted_line[7].split('"')
                recv_nonce = aux[1]

                # no lo registramos si no coincide el digest_nonce
                if hl_nonce != recv_nonce:
                    self.nonce_dictionary[user_name] = str(random.randint(0, 100000000))
                    line_to_send += 'SIP/2.0 401 Unauthorized'
                    line_to_send += '\r\n'
                    line_to_send += 'WWW Authenticate: Digest nonce="'
                    line_to_send += self.nonce_dictionary[user_name]
                    line_to_send += '"\r\n\r\n'

                    """
                    Lo enviamos al cliente
                    """
                    self.send_to_client(CLIENT_IP_ADDRESS, CLIENT_PORT_NUMBER, line_to_send)

                # lo registramos si coincide el digest_nonce
                else:
                    if splitted_line[4] == '0':
                        del self.register_dictionary[user_name]
                        line_to_send ='SIP/2.0 200 OK\r\n\r\n'

                        """
                        Lo enviamos al cliente
                        """
                        self.send_to_client(CLIENT_IP_ADDRESS, CLIENT_PORT_NUMBER, line_to_send)

                    else:
                        line_to_send = 'SIP/2.0 200 OK\r\n\r\n'

                        self.register_dictionary[user_name] = {'ip': CLIENT_IP_ADDRESS,
                                               'expires': expiration_time,
                                               'puerto': port,
                                               'registro': current_time}


                        """
                        Lo enviamos al cliente
                        """
                        self.send_to_client(CLIENT_IP_ADDRESS, CLIENT_PORT_NUMBER, line_to_send)

        else:

            # lo escribimos en algo
            line = 'SIP/2.0 400 Bad Request\r\n\r\n'
            byted_line = bytes(line, 'utf-8')
            self.wfile.write(byted_line)

            # lo escribimos en el fichero de log
            line_to_write = 'Error: SIP/2.0 400 Bad Request'
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            line_to_show += '\r\n\r\nError: SIP/2.0 400 Bad Request\r\n'
            print(line_to_show)

        self.register2json()

def write_log(eventt, log_path):
    """
    Escribe los mensajes para poder trazar que es lo que ha ocurrido
    (o esta ocurriendo) en el User Agent
    Tiene que tener la siguiente estructura
    Tiempo_en_formato_YYYYMMDDHHMMSS (espacio en blanco) Evento
    """
    
    # abrimos el fichero con 'a' (Append)
    # si el fichero no existiese, lo crearia
    fich = open(log_path, 'a')
    
    # pasamos el tiempo a un string en el formato especificado
    timee = time.strftime('%Y%m%d%H%M%S')
    fich.write(timee)
    fich.write(' ')
    fich.write(eventt)
    fich.write('\r\n')
    fich.close()


if __name__ == "__main__":
    """
    Programa principal
    """
    usagee = 'Usage: python proxy_registrar.py config'

    # si no se introduce el numero de parametros correctos debe mostrar el mensaje
    if len(sys.argv) != 2:
        sys.exit(usagee)

    # argumentos. CONFIG es el fichero xml
    # sigue la nomenclatura descrita en el mensaje Usage
    try:
        _, CONFIG = sys.argv

    # en caso de error en los parametros debe mostrar el mensaje
    except IndexError:
        sys.exit(usagee)

    parser = make_parser()
    prh = ProxyRegistrarHandler()
    parser.setContentHandler(prh)

    # CONFIG es el fichero xml
    try:
        parser.parse(open(CONFIG))

    # si no encuentra el fichero muestra el mensaje
    except FileNotFoundError:
        sys.exit(usagee)

    # volcamos el contenido del fichero xml en el diccionario
    config_dictionary = prh.get_tags()

    """
    Volcamos el diccionario en variables
    """

    SERVER_NAME = config_dictionary['server_name']
    SERVER_PUERTO = int(config_dictionary['server_puerto'])
    DATABASE_PATH = config_dictionary['database_path']
    DATABASE_PASSWDPATH = config_dictionary['database_passwdpath']
    LOG_PATH = config_dictionary['log_path']

    # en caso de que en el xml no se especifique la direccion IP del servidor
    # usamos la 127.0.0.1 tal y como se detalla en el pdf
    SERV_IP = '127.0.0.1'
    if config_dictionary['server_ip'] != '':
        # si viene especificada, volcamos su valor en la variable
        SERV_IP = config_dictionary['server_ip']

    """
    Servidor
    """

    servv = socketserver.UDPServer((SERV_IP, SERVER_PUERTO), SIPRegisterHandler)
    print('Starting...')
    line_to_show = 'Server '
    line_to_show += SERVER_NAME
    line_to_show += ' listening at port '
    line_to_show += str(SERVER_PUERTO)
    print(line_to_show)

    # lo escribimos en el fichero de log
    line_to_write = 'Starting...'
    write_log(line_to_write, LOG_PATH)

    try:
        servv.serve_forever()

    except KeyboardInterrupt:

        """
        Fin del programa
        """

        # lo escribimos en el fichero de log
        line_to_write = 'Finishing.'
        write_log(line_to_write, LOG_PATH)

        # lo mostramos por pantalla
        line_to_show = 'Finishing.'
        print(line_to_show)
