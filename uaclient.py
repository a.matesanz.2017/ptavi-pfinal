#!/usr/bin/python3

import hashlib
import os
import socket
import sys
import time

from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class UserAgentHandler(ContentHandler):
    """
    Clase para manejar los elementos de los ficheros xml
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """

        # los elementos siguen la sintaxis descrita en el DTD
        self.account_list = ['username', 'passwd']
        self.uaserver_list = ['ip', 'puerto']
        self.rtpaudio_list = ['puerto']
        self.regproxy_list = ['ip', 'puerto']
        self.log_list = ['path']
        self.audio_list = ['path']

        self.base_ua_dictionary = {'account': self.account_list,
                                   'uaserver': self.uaserver_list,
                                   'rtpaudio': self.rtpaudio_list,
                                   'regproxy': self.regproxy_list,
                                   'log': self.log_list,
                                   'audio': self.audio_list
                                   }

        self.ua_dictionary = {}

    def startElement(self, name, attrs):
        """
        Introducimos los elementos del fichero xml en el diccionario
        """

        if name in self.base_ua_dictionary:
            for attrib in self.base_ua_dictionary[name]:
                n_a = name
                n_a += '_'
                n_a += attrib
                self.ua_dictionary[n_a] = attrs.get(attrib, '')

    def get_tags(self):
        return self.ua_dictionary

def write_log(eventt, log_path):
    """
    Escribe los mensajes para poder trazar que es lo que ha ocurrido
    (o esta ocurriendo) en el User Agent
    Tiene que tener la siguiente estructura
    Tiempo_en_formato_YYYYMMDDHHMMSS (espacio en blanco) Evento
    """
    
    # abrimos el fichero con 'a' (Append)
    # si el fichero no existiese, lo crearia
    fich = open(log_path, 'a')
    
    # pasamos el tiempo a un string en el formato especificado
    timee = time.strftime('%Y%m%d%H%M%S')
    fich.write(timee)
    fich.write(' ')
    fich.write(eventt)
    fich.write("\r\n")
    fich.close()


if __name__ == "__main__":
    """
    Programa principal
    """
    # mensaje descrito en el pdf
    usagee = 'Usage: python uaclient.py config method option'
    wrong_method = 'Error, wrong method. It has to be REGISTER, INVITE or BYE'
    wrong_method += '\r\n'
    wrong_method += usagee
    not_file = 'File not found'
    not_file += '\r\n'
    not_file += usagee

    # si no se introduce el numero de parametros correctos debe mostrar el mensaje
    if len(sys.argv) != 4:
        sys.exit(usagee)

    # argumentos. CONFIG es el fichero xml
    # sigue la nomenclatura descrita en el mensaje Usage
    try:
        _, CONFIG, METHOD, OPTION = sys.argv
        METHOD = METHOD.upper()

        if METHOD != 'REGISTER' and METHOD != 'INVITE' and METHOD != 'BYE':
            sys.exit(wrong_method)

    # en caso de error en los parametros debe mostrar el mensaje
    except IndexError:
        sys.exit(usagee)

    parser = make_parser()
    uah = UserAgentHandler()
    parser.setContentHandler(uah)

    # CONFIG es el fichero xml
    try:
        parser.parse(open(CONFIG))

    # si no encuentra el fichero muestra el mensaje
    except FileNotFoundError:
        sys.exit(not_file)

    # volcamos el contenido del fichero xml en el diccionario
    config_dictionary = uah.get_tags()

    """
    Volcamos el diccionario en variables
    """

    # esto puerto solo va a usarse como string asi que lo convertimos directamente
    RTPAUDIO_PUERTO = str(config_dictionary['rtpaudio_puerto'])

    ACCOUNT_USERNAME = str(config_dictionary['account_username'])
    ACCOUNT_PASSWD = config_dictionary['account_passwd']
    UASERVER_PUERTO = config_dictionary['uaserver_puerto']
    REGPROXY_PUERTO = int(config_dictionary['regproxy_puerto'])
    LOG_PATH = config_dictionary['log_path']
    AUDIO_PATH = config_dictionary['audio_path']

    # en caso de que en el xml no se especifique la direccion IP del servidor
    # usamos la 127.0.0.1 tal y como se detalla en el pdf
    UAS_IP = '127.0.0.1'
    if config_dictionary['uaserver_ip'] != '':
        # si viene especificada, volcamos su valor en la variable
        UAS_IP = config_dictionary['uaserver_ip']

    # lo mismo para la direccion IP del proxy_registrar
    PR_IP = '127.0.0.1'
    if config_dictionary['regproxy_ip'] != '':
        PR_IP = config_dictionary['regproxy_ip']

    # lo escribimos en el fichero de log
    line_to_write = 'Starting...'
    write_log(line_to_write, LOG_PATH)

    # lo mostramos por pantalla
    line_to_show = 'Starting...'
    print(line_to_show)

    """
    Socket
    """
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sockett:
        sockett.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sockett.connect((PR_IP, REGPROXY_PUERTO))

        """
        Ahora comparamos el metodo
        """
        if METHOD == 'REGISTER':
            line = METHOD
            line += ' sip:' + ACCOUNT_USERNAME
            line += ':' + UASERVER_PUERTO
            line += ' SIP/2.0\r\n'
            line += 'Expires: ' + OPTION + '\r\n\r\n\r\n'

        elif METHOD == 'INVITE':
            line = METHOD
            line += ' sip:' + OPTION
            line += ' SIP/2.0\r\n'
            line += 'Content-Type: application/sdp\r\n\r\n'
            line += 'v=0\r\n'
            line += 'o=' + ACCOUNT_USERNAME + ' ' + UAS_IP + '\r\n'
            line += 's=Contacting\r\n'
            line += 't=0\r\n'
            line += 'm=audio ' + RTPAUDIO_PUERTO + ' RTP\r\n\r\n\r\n'

        elif METHOD == 'BYE':
            line = METHOD
            line += ' sip:' + OPTION
            line += ' SIP/2.0\r\n\r\n\r\n'

        else:
            line = METHOD
            line += ' sip:' + OPTION
            line += ' SIP/2.0\r\n\r\n\r\n'

        byted_line = bytes(line, 'utf-8')
        sockett.send(byted_line)

        # lo escribimos en el fichero de log
        corrected_line = line.replace('\r\n', ' ')
        line_to_write = 'Sent to ' + PR_IP
        line_to_write += ':' + str(REGPROXY_PUERTO)
        line_to_write += ': ' + corrected_line
        write_log(line_to_write, LOG_PATH)

        # lo mostramos por pantalla
        tuplee = '(IP address: ' + PR_IP
        tuplee += ', Port number: ' + str(REGPROXY_PUERTO) + ')'
        stripped_line = line.lstrip()
        line_to_show = '   Sent to Proxy '
        line_to_show += tuplee + ':'
        print('\r\n\r\n' + line_to_show)
        print('\r\n' + stripped_line)

        """
        Cliente
        """

        try:
            recv_data = sockett.recv(1024)
        except ConnectionRefusedError:

            # lo escribimos en el fichero de log
            # usando la estructura especificada en el pdf
            error_message = 'Error: No server listening at '
            error_message += SERVER_PROXY
            error_message += ' port '
            error_message += str(REGPROXY_PUERTO)
            write_log(error_message, LOG_PATH)

            # lo mostramos por pantalla
            line_to_show = 'Error: No server listening at that port'
            sys.exit(line_to_show)

        decoded_recv = recv_data.decode('utf-8')

        # lo escribimos en el fichero de log
        corrected_recv = decoded_recv.replace('\r\n', ' ')
        line_to_write = 'Received from ' + PR_IP
        line_to_write += ':' + str(REGPROXY_PUERTO)
        line_to_write += ': ' + corrected_recv
        write_log(line_to_write, LOG_PATH)

        # lo mostramos por pantalla
        tuplee = '(IP address: '
        tuplee += PR_IP
        tuplee += ', Port number: '
        tuplee += str(REGPROXY_PUERTO)
        tuplee += ')'
        stripped_recv = decoded_recv.lstrip()
        line_to_show = '   Received from Proxy '
        line_to_show += tuplee + ':'
        print('\r\n\r\n' + line_to_show)
        print('\r\n' + stripped_recv)

        """
        Ahora comparamos los codigos de respuesta sip
        """

        splitted_recv = decoded_recv.split()
        sip_code = splitted_recv[1]

        if sip_code == '400' or sip_code == '404' or sip_code == '405':

            # lo escribimos en el fichero de log
            line_to_write = 'Error: '
            line_to_write += decoded_recv
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            line_to_show = 'Error: '
            line_to_show += decoded_recv
            print('\r\n' + line_to_show)

        elif sip_code == '401':
            recv_nonce = splitted_recv[6].split('"')[1]

            hl_md5 = hashlib.md5()
            passwdd = bytes(ACCOUNT_PASSWD, 'utf-8')
            hl_md5.update(passwdd)
            b_nonce = bytes(recv_nonce, 'utf-8')
            hl_md5.update(b_nonce)
            hl_nonce = hl_md5.hexdigest()

            line = METHOD
            line += ' sip:' + ACCOUNT_USERNAME
            line += ':' + UASERVER_PUERTO
            line += ' SIP/2.0\r\n'
            line += 'Expires: ' + OPTION + '\r\n'
            line += 'Authorization: Digest response="' + hl_nonce + '"\r\n\r\n'

            byted_line = bytes(line, 'utf-8')
            sockett.send(byted_line)

            # lo escribimos en el fichero de log
            corrected_line = line.replace('\r\n', ' ')
            line_to_write = 'Sent to ' + PR_IP
            line_to_write += ':' + str(REGPROXY_PUERTO)
            line_to_write += ': ' + corrected_line
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            tuplee = '(IP address: ' + PR_IP
            tuplee += ', Port number: ' + str(REGPROXY_PUERTO) + ')'
            stripped_line = line.lstrip()
            line_to_show = '   Sent to Proxy '
            line_to_show += tuplee + ':'
            print('\r\n\r\n' + line_to_show)
            print('\r\n' + stripped_line)

            recv_data = sockett.recv(1024)
            decoded_recv = recv_data.decode('utf-8')

            # lo escribimos en el fichero de log
            corrected_recv = decoded_recv.replace('\r\n', ' ')
            line_to_write = 'Received from ' + PR_IP
            line_to_write += ':' + str(REGPROXY_PUERTO)
            line_to_write += ': ' + corrected_recv
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            tuplee = '(IP address: ' + PR_IP
            tuplee += ', Port number: ' + str(REGPROXY_PUERTO) + ')'
            stripped_recv = decoded_recv.lstrip()
            line_to_show = '   Received from Proxy '
            line_to_show += tuplee + ':'
            print('\r\n\r\n' + line_to_show)
            print('\r\n' + stripped_recv)

        elif sip_code == '100' and splitted_recv[4] == '180' and splitted_recv[7] == '200':
            ip_address = splitted_recv[16]
            rtp_port = splitted_recv[19]

            line = 'ACK sip:'
            line += OPTION + ' SIP/2.0\r\n\r\n'

            byted_line = bytes(line, 'utf-8')
            sockett.send(byted_line)

            # lo escribimos en el fichero de log
            corrected_line = line.replace('\r\n', ' ')
            line_to_write = 'Sent to ' + PR_IP
            line_to_write += ':' + str(REGPROXY_PUERTO)
            line_to_write += ': ' + corrected_line
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            tuplee = '(IP address: ' + PR_IP
            tuplee += ', Port number: ' + str(REGPROXY_PUERTO) + ')'
            stripped_line = line.lstrip()
            line_to_show = '   Sent to Proxy '
            line_to_show += tuplee + ':'
            print('\r\n\r\n' + line_to_show)
            print('\r\n' + stripped_line)

            # envio RTP mediante el programa mp32rtp
            # el string indica lo que hay que poner en la shell
            line = 'mp32rtp -i '
            line += ip_address
            line += ' -p '
            line += rtp_port
            line += ' < '
            line += AUDIO_PATH

            os.system(line)

            # lo escribimos en el fichero de log
            line_to_write = 'Sent to ' + ip_address
            line_to_write += ':' + rtp_port
            line_to_write += ': ' + line
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            line_to_show = '   RTP sent to server ' + ip_address
            line_to_show += ' in ' + rtp_port
            print('\r\n\r\n' + line_to_show)

        """
        Fin del programa
        """

        # lo escribimos en el fichero de log
        line_to_write = 'Finishing.'
        write_log(line_to_write, LOG_PATH)

        # lo mostramos por pantalla
        line_to_show = '\r\n'
        line_to_show += 'Finishing.'
        print(line_to_show)
