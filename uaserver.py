#!/usr/bin/python3

import os
import socketserver
import sys
import time

from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class UserAgentHandler(ContentHandler):
    """
    Clase para manejar los elementos de los ficheros xml
    """

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """

        # los elementos siguen la sintaxis descrita en el DTD
        self.account_list = ['username', 'passwd']
        self.uaserver_list = ['ip', 'puerto']
        self.rtpaudio_list = ['puerto']
        self.regproxy_list = ['ip', 'puerto']
        self.log_list = ['path']
        self.audio_list = ['path']

        self.base_ua_dictionary = {'account': self.account_list,
                                   'uaserver': self.uaserver_list,
                                   'rtpaudio': self.rtpaudio_list,
                                   'regproxy': self.regproxy_list,
                                   'log': self.log_list,
                                   'audio': self.audio_list
                                   }

        self.ua_dictionary = {}

    def startElement(self, name, attrs):
        """
        Introducimos los elementos del fichero xml en el diccionario
        """

        if name in self.base_ua_dictionary:
            for attrib in self.base_ua_dictionary[name]:
                n_a = name
                n_a += '_'
                n_a += attrib
                self.ua_dictionary[n_a] = attrs.get(attrib, '')

    def get_tags(self):
        return self.ua_dictionary


class HandlerServer(socketserver.DatagramRequestHandler):

    # lista que contendra ip y puerto
    decoded_dir = []

    def handle(self):

        # con la tupla client_address obtenemos la IP y el puerto del cliente
        ip = self.client_address[0]
        CLIENT_IP_ADDRESS = str(ip)
        port = self.client_address[1]
        CLIENT_PORT_NUMBER = str(port)

        # bucle para escribir en el fichero de log
        # creamos e inicializamos la variable donde vamos a ir decodificando
        decoded_line = ''
        for splitted_line in self.rfile:

            # lo escribimos en el fichero de log
            decoded_line += splitted_line.decode('utf-8')
            corrected_line = decoded_line.replace('\r\n', ' ')
            line_to_write = 'Received from '
            line_to_write += PR_IP
            line_to_write += ':'
            line_to_write += REGPROXY_PUERTO
            line_to_write += ': '
            line_to_write += corrected_line
            write_log(line_to_write, LOG_PATH)

            splitted_line = decoded_line.split()

        # lo mostramos por pantalla
        tuplee = '(IP address: '
        tuplee += PR_IP
        tuplee += ', Port number: '
        tuplee += REGPROXY_PUERTO
        tuplee += ')'
        line_to_show = '   Received from Proxy '
        line_to_show += tuplee
        line_to_show += ':'
        print('\r\n\r\n' + line_to_show)
        print('\r\n' + decoded_line)

        """
        Ahora comparamos el metodo
        """
        CONFIGG = splitted_line[0]

        if CONFIGG != 'INVITE' and CONFIGG != 'ACK' and CONFIGG != 'BYE':
            line = 'SIP/2.0 405 Method Not Allowed\r\n\r\n\r\n'
            byted_line = bytes(line, 'utf-8')
            self.wfile.write(byted_line)

            # lo escribimos en el fichero de log
            corrected_line = line.replace('\r\n', ' ')
            line_to_write = 'Sent to '
            line_to_write += CLIENT_IP_ADDRESS
            line_to_write += ':'
            line_to_write += CLIENT_PORT_NUMBER
            line_to_write += ': '
            line_to_write += corrected_line
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            tuplee = '(IP address: '
            tuplee += CLIENT_IP_ADDRESS
            tuplee += ', Port number: '
            tuplee += CLIENT_PORT_NUMBER
            tuplee += ')'
            stripped_line = line.lstrip()
            line_to_show = '   Sent to Proxy '
            line_to_show += tuplee
            line_to_show += ':'
            print('\r\n\r\n' + line_to_show)
            print('\r\n' + stripped_line)

        elif CONFIGG == 'BYE':
            line = 'SIP/2.0 200 OK\r\n\r\n\r\n'
            byted_line = bytes(line, 'utf-8')
            self.wfile.write(byted_line)

            # lo escribimos en el fichero de log
            corrected_line = line.replace('\r\n', ' ')
            line_to_write = 'Sent to '
            line_to_write += CLIENT_IP_ADDRESS
            line_to_write += ':'
            line_to_write += CLIENT_PORT_NUMBER
            line_to_write += ': '
            line_to_write += corrected_line
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            tuplee = '(IP address: '
            tuplee += CLIENT_IP_ADDRESS
            tuplee += ', Port number: '
            tuplee += CLIENT_PORT_NUMBER
            tuplee += ')'
            stripped_line = line.lstrip()
            line_to_show = '   Sent to Proxy '
            line_to_show += tuplee
            line_to_show += ':'
            print('\r\n\r\n' + line_to_show)
            print('\r\n' + stripped_line)

        elif CONFIGG == 'ACK':
            """
            Envio RTP mediante el programa mp32rtp
            """

            # el string indica lo que hay que poner en la shell
            line = 'mp32rtp -i '
            line += str(self.decoded_dir[0])
            line += ' -p '
            line += str(self.decoded_dir[1])
            line += ' < '
            line += AUDIO_PATH
            os.system(line)

            # lo escribimos en el fichero de log
            line_to_write = 'Sent to '
            line_to_write += self.decoded_dir[0]
            line_to_write += ':'
            line_to_write += str(self.decoded_dir[1])
            line_to_write += ': '
            line_to_write += line
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            tuplee = '(IP address: '
            tuplee += self.decoded_dir[0]
            tuplee += ', Port number: '
            tuplee += str(self.decoded_dir[1])
            tuplee += ')'
            line_to_show = '   Sent to Client '
            line_to_show += tuplee
            line_to_show += ':'
            print('\r\n\r\n' + line_to_show)
            print('\r\n' + line)

        elif CONFIGG == 'INVITE':

            # En la lista previamente creada introducimos la IP y el puerto
            self.decoded_dir.append(splitted_line[10])
            self.decoded_dir.append(splitted_line[14])

            line = 'SIP/2.0 100 Trying'
            line += '\r\n\r\nSIP/2.0 180 Ringing'
            line += '\r\n\r\nSIP/2.0 200 OK'
            line += '\r\nContent-Type: application/sdp'
            line += '\r\n\r\nv=0'
            line += '\r\no='
            line += ACCOUNT_USERNAME
            line += ' '
            line += UAS_IP
            line += '\r\ns=PleasantSurprise'
            line += '\r\nt=0'
            line += '\r\nm=audio '
            line += RTPAUDIO_PUERTO
            line += ' RTP'
            line += '\r\n\r\n\r\n'

            byted_line = bytes(line, 'utf-8')
            self.wfile.write(byted_line)

            # lo escribimos en el fichero de log
            corrected_line = line.replace('\r\n', ' ')
            line_to_write = 'Sent to '
            line_to_write += CLIENT_IP_ADDRESS
            line_to_write += ':'
            line_to_write += CLIENT_PORT_NUMBER
            line_to_write += ': '
            line_to_write += corrected_line
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            tuplee = '(IP address: '
            tuplee += CLIENT_IP_ADDRESS
            tuplee += ', Port number: '
            tuplee += CLIENT_PORT_NUMBER
            tuplee += ')'
            stripped_line = line.lstrip()
            line_to_show = '   Sent to Proxy '
            line_to_show += tuplee
            line_to_show += ':'
            print('\r\n\r\n' + line_to_show)
            print('\r\n' + stripped_line)

        else:
            line = 'SIP/2.0 400 Bad Request\r\n\r\n\r\n'
            byted_line = bytes(line, 'utf-8')
            self.wfile.write(byted_line)

            # lo escribimos en el fichero de log
            line_to_write = 'Error: SIP/2.0 400 Bad Request'
            write_log(line_to_write, LOG_PATH)

            # lo mostramos por pantalla
            line_to_show = 'Error: SIP/2.0 400 Bad Request'
            print('\r\n\r\n' + line_to_show)

def write_log(eventt, log_path):
    """
    Escribe los mensajes para poder trazar que es lo que ha ocurrido
    (o esta ocurriendo) en el User Agent
    Tiene que tener la siguiente estructura
    Tiempo_en_formato_YYYYMMDDHHMMSS (espacio en blanco) Evento
    """
    
    # abrimos el fichero con 'a' (Append)
    # si el fichero no existiese, lo crearia
    fich = open(log_path, 'a')
    
    # pasamos el tiempo a un string en el formato especificado
    timee = time.strftime('%Y%m%d%H%M%S')
    fich.write(timee)
    fich.write(' ')
    fich.write(eventt)
    fich.write('\r\n')
    fich.close()


if __name__ == "__main__":
    """
    Programa principal
    """
    usagee = 'Usage: python uaserver.py config'
    not_file = 'File not found\r\n' + usagee

    # si no se introduce el numero de parametros correctos debe mostrar el mensaje
    if len(sys.argv) != 2:
        sys.exit(usagee)

    # argumentos. CONFIG es el fichero xml
    # sigue la nomenclatura descrita en el mensaje Usage
    try:
        _, CONFIG = sys.argv

    # en caso de error en los parametros debe mostrar el mensaje
    except IndexError:
        sys.exit(usagee)

    parser = make_parser()
    uah = UserAgentHandler()
    parser.setContentHandler(uah)

    # CONFIG es el fichero xml
    try:
        parser.parse(open(CONFIG))

    # si no encuentra el fichero muestra el mensaje
    except FileNotFoundError:
        sys.exit(not_file)

    # volcamos el contenido del fichero xml en el diccionario
    config_dictionary = uah.get_tags()

    """
    Volcamos el diccionario en variables
    """

    # estos puertos solo van a usarse como strings asi que los convertimos directamente
    RTPAUDIO_PUERTO = str(config_dictionary['rtpaudio_puerto'])
    REGPROXY_PUERTO = str(config_dictionary['regproxy_puerto'])

    ACCOUNT_USERNAME = config_dictionary['account_username']
    UASERVER_PUERTO = int(config_dictionary['uaserver_puerto'])
    LOG_PATH = config_dictionary['log_path']
    AUDIO_PATH = config_dictionary['audio_path']

    # en caso de que en el xml no se especifique la direccion IP del servidor
    # usamos la 127.0.0.1 tal y como se detalla en el pdf
    UAS_IP = '127.0.0.1'
    if config_dictionary['uaserver_ip'] != '':
        # si viene especificada, volcamos su valor en la variable
        UAS_IP = config_dictionary['uaserver_ip']

    # lo mismo para la direccion IP del proxy_registrar
    PR_IP = '127.0.0.1'
    if config_dictionary['regproxy_ip'] != '':
        PR_IP = config_dictionary['regproxy_ip']

    """
    Servidor
    """

    servv = socketserver.UDPServer((UAS_IP, UASERVER_PUERTO), HandlerServer)

    # lo escribimos en el fichero de log
    line_to_write = 'Starting...'
    write_log(line_to_write, LOG_PATH)

    # lo mostramos por pantalla
    line_to_show = 'Starting...'
    line_to_show += '\r\n'
    line_to_show += 'Listening...'
    print(line_to_show)


    try:
        servv.serve_forever()

    except KeyboardInterrupt:

        """
        Fin del programa
        """

        # lo escribimos en el fichero de log
        line_to_write = 'Finishing.'
        write_log(line_to_write, LOG_PATH)

        # lo mostramos por pantalla
        line_to_show = 'Finishing.'
        print(line_to_show)
